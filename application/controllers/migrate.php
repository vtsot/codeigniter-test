<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migrate extends CI_Controller
{

    /**
     * 
     */
    public function index()
    {

        $this->load->library('migration');
        $this->load->dbforge();
        
//        print '<pre>'; var_dump(get_class($this->migration)); print '</pre>';
//        die('<br/>line:'.__LINE__.'<br/>file: '.__FILE__);
        if (!$this->migration->version(1)) {
            show_error($this->migration->error_string());
        }
    }

}
